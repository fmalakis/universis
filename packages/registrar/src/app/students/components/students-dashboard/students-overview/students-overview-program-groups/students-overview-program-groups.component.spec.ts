import { CommonModule } from '@angular/common';
import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MostModule } from '@themost/angular';
import { SharedModule } from '@universis/common';
import { AngularDataContext } from '@themost/angular';
import { StudentsOverviewProgramGroupsComponent } from './students-overview-program-groups.component';

describe('StudentsOverviewProgramGroupsComponent', () => {
  let component: StudentsOverviewProgramGroupsComponent;
  let fixture: ComponentFixture<StudentsOverviewProgramGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        CommonModule,
        SharedModule,
      ],
      declarations: [ StudentsOverviewProgramGroupsComponent ],
      providers: [
        AngularDataContext
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsOverviewProgramGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject([AngularDataContext], (_context: AngularDataContext) => {
    expect(component).toBeTruthy();
  }));
});
