import { Injectable } from "@angular/core";
import { AngularDataContext } from "@themost/angular";

export declare interface ApiServerStatus {
  version: string;
  database: string;
  modifiedAt?: Date;
  attachedAt?: Date;
}

export declare interface ServiceDefinition {
  serviceType: string;
  strategyType?: string;
}

/**
 *
 * This Service is used to get diagnostics from the api server
 * @export
 * @class DiagnosticsService
 */

@Injectable()
export class DiagnosticsService {

  private _services: Array<ServiceDefinition>;

  constructor(private context: AngularDataContext) { }

  /**
   *
   * Get status of api server
   * @returns {Promise<ApiServerStatus>} - Returns ApiServerStatus promise with version, database and optional modifiedAt/attachedAt dates.
   * @memberof DiagnosticsService
   */
  getStatus(): Promise<ApiServerStatus> {
    return this.context.getService().execute({
      method: 'GET',
      url: 'diagnostics/status',
      headers: { },
      data: null
    });
  }

  async getServices(): Promise<Array<ServiceDefinition>> {
    if (this._services) {
      return this._services;
    }
    const results = await this.context.getService().execute({
      method: 'GET',
      url: 'diagnostics/services',
      headers: {},
      data: null
    });
    this._services = results;
    return results;
  }

  refresh() {
    delete this._services;
  }

  async hasService(serviceType: string): Promise<boolean> {
    const results = await this.getServices();
    return results.findIndex((item) => {
      return item.serviceType === serviceType;
    }) > -1;
  }

  async hasStrategy(serviceType: string, strategyType: string): Promise<boolean> {
    const results = await this.getServices();
    return results.findIndex((item) => {
      return item.serviceType === serviceType &&
        item.strategyType === strategyType;
    }) > -1;
  }

}
