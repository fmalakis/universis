import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-instructors-dashboard-overview-profile',
  templateUrl: './instructors-dashboard-overview-profile.component.html',
  styleUrls: ['./instructors-dashboard-overview-profile.component.scss']
})
export class InstructorsDashboardOverviewProfileComponent implements OnInit, OnDestroy {
  public model: any;
  public instructorId: any;
  private fragmentSubscription: Subscription;
  @Input() id: any;
  public showMessageForm = false;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    try {
      this.model = await this.dataModel();
    } catch (err) {
      console.log(err);
    }
    this.instructorId = this._activatedRoute.snapshot.params.id;
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async fragment => {
      if (fragment && fragment === 'reload') {
        try {
          this.model = await this.dataModel();
        } catch (err) {
          console.log(err);
        }
      }
    });
  }

  dataModel() {
    return this._context.model('Instructors')
    .where('id').equal(this._activatedRoute.snapshot.params.id)
    .expand('user,department($expand=organization)')
    .getItem();
  }

  enableMessages() {
    this.showMessageForm = !this.showMessageForm;
  }

  onsuccesfulSend(succesful: boolean) {
    this.showMessageForm = !succesful;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

}
