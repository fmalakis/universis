import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 *
 * Component for Card-Box element with Inputs:
 * Usage <universis-msgbox [title]="HelloWorld" ...></universis-msgbox>
 * @Input() title: Title of Box
 * @Input() icon: Icon displayed on the left of the element
 * @Input() info: The Status displayed on user
 * @Input() message: Explanation of the status
 * @Input() extraMessage: Some extra guidence
 * @Input() actionButton: Text displayed as Text in button
 * @Input() actionText: Text displayed as an action
 * @Input() disableBut: Disable button
 * @export
 */
@Component({
  selector: 'universis-msgbox',
  templateUrl: './msgbox.component.html',
  styleUrls: ['./msgbox.component.scss']
})
export class MsgboxComponent {

  @Input() title: string;
  @Input() icon: string;
  @Input() info: string;
  @Input() message: string;
  @Input() extraMessage: string;
  @Input() actionButton: string;
  @Input() actionText: string;
  @Input() disableBut: boolean;
  // Default class sets the color to green, otherwise pass it the correct bootstrap class
  @Input() buttonClass = 'btn-success';
  // Usage (action)="someFunction()"
  @Output() action = new EventEmitter<any>();

  btnClicked: boolean;

  clicked() {
    this.action.emit();
  }
}
