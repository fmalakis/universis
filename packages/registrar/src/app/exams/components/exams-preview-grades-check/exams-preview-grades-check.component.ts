import {Component, OnDestroy, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {
  AppEventService,
  ConfigurationService,
  DIALOG_BUTTONS,
  ErrorService,
  LoadingService,
  CertificateService,
  ModalService, SignatureInfoComponent, ToastService
} from '@universis/common';
import { BsModalRef } from 'ngx-bootstrap';
import {ApplicationSettings} from '../../../registrar-shared/registrar-shared.module';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '@universis/ngx-tables';
import * as EXAM_GRADES_LIST_CONFIG from './action-grades-table.config.json';
import {ActivatedTableService} from '@universis/ngx-tables';


@Component({
  selector: 'app-exams-preview-grades-check',
  templateUrl: './exams-preview-grades-check.component.html',
  styleUrls: ['./exams-preview-grades-check.component.scss']
})
export class ExamsPreviewGradesCheckComponent implements OnInit, OnDestroy  {

  public courseExamId;
  public courseExamAction: any;
  public instructor: any;
  public useDigitalSignature = true;
  private subscription: Subscription;
  public actionId;
  private changeSubscription: Subscription;
  @ViewChild('grades') grades: AdvancedTableComponent;

  /**
   * @property {boolean} signatureVerified Indicates whether the signature was verified
   */
  public signatureVerified: boolean;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private errorService: ErrorService,
              private _config: ConfigurationService,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _appEvent: AppEventService,
              private _toastService: ToastService,
              private _activatedTable: ActivatedTableService,
              private _certificateService: CertificateService

  ) { }

  async ngOnInit() {
    try {
      this._loadingService.showLoading();
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        // get digital signature settings from app config file
        this.useDigitalSignature = !!(<ApplicationSettings>this._config.settings.app).useDigitalSignature;
        this.courseExamId = this._activatedRoute.snapshot.parent.params.id;
        this.actionId = params.id;
        await this.load();

      });
      this.changeSubscription = this._appEvent.change.subscribe(async (event) => {
        if (event && (event.model === 'ExamDocumentUploadActions')
          && event.target && this.courseExamAction && event.target.id === this.courseExamAction.id) {
          // reload
          await this.load();

        }
      });
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    }

  }
private async load() {
  this.courseExamAction = await this._context.model(`CourseExams/${this.courseExamId}/actions`)
    .asQueryable()
    .expand('additionalResult,attachments,grades,owner,object($expand=examPeriod,' +
      'course($expand=department($expand=currentPeriod,currentYear))' +
      ',classes($expand=courseClass($expand=period,year)),examPeriod,year),actionResult($expand=createdBy,actionStatus)')
    .where('id').equal(this.actionId)
    .getItem();
  if (this.courseExamAction.owner) {
    this.instructor = await this._context.model('Instructors')
      .where('user').equal(this.courseExamAction.owner.id)
      .getItem();
  }
  // get related requestDocumentActions
  if (this.courseExamAction.grades && this.courseExamAction.grades.length > 0) {
    this.getActionGrades();
  }
  this.signatureVerified = false;
}
  attachedGrades(attachments) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachments[0].url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachments[0].name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  getActionGrades() {
    this._activatedTable.activeTable = this.grades;
    this.grades.query = this._context.model(`ExamDocumentUploadActions/${this.courseExamAction.id}/grades`)
      .asQueryable()
      .expand('status')
      .prepare();
    this.grades.config = AdvancedTableConfiguration.cast(EXAM_GRADES_LIST_CONFIG);
    this.grades.ngOnInit();

  }

  async changeStatus(status) {
    try {
      const title = status === 'approve' ?
        this._translateService.instant('Exams.AcceptGrades.title') :
        this._translateService.instant('Exams.RejectGrades.title');
      const message = status === 'approve' ?
        this._translateService.instant('Exams.AcceptGrades.message') :
        this._translateService.instant('Exams.RejectGrades.message');

      const dialogResult = await this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo);
      if (dialogResult === 'no') {
        return;
      }
      this._loadingService.showLoading();

      // and finally save action
      await this._context.model(`ExamDocumentUploadActions/${this.actionId}/${status}`).save(null);
      this._appEvent.change.next({
        model: 'ExamDocumentUploadActions',
        target: this.courseExamAction
      });
      this._toastService.show(
        this._translateService.instant(status === 'approve' ? `Exams.AcceptGrades.title` : `Exams.RejectGrades.title`),
        this._translateService.instant(status === 'approve' ? `Exams.AcceptGrades.success` : `Exams.RejectGrades.success`),
      );
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      console.log(err);
      return this.errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   *
   * Opens the certification modal
   *
   */
  showCertificateModal(): void {
    this._modalService.openModalComponent(
      SignatureInfoComponent,
      {
        animated: true,
        class: 'modal-dialog-centered modal-xl border-0',
        ignoreBackdropClick: true,
        initialState: {
          isVerified: this.signatureVerified,
          user: this.instructor,
          certificate: this.courseExamAction.additionalResult.userCertificate,
          signatureBlock: this.courseExamAction.additionalResult.signatureBlock,
          checkHashKey: this.courseExamAction.additionalResult.checkHashKey,
          examinationCode: this.courseExamAction.code,
          dateCreated: this.courseExamAction.dateCreated
        }
      }
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }
}
