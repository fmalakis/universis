import {en} from './error.en';
import {el} from './error.el';

export const ERROR_LOCALES = {
    el: el,
    en: en
};
