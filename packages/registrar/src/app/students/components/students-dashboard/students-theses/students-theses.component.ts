import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import * as STUDENTS_THESES_LIST_CONFIG from './students-theses.config.list.json';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
@Component({
  selector: 'app-students-theses',
  templateUrl: './students-theses.component.html'
})
export class StudentsThesesComponent implements OnInit, OnDestroy  {
  @ViewChild('model') model: AdvancedTableComponent;
  private subscription: Subscription;
  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>STUDENTS_THESES_LIST_CONFIG;
  public recordsTotal: any;
  @Input() tableConfiguration: any;
  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this._activatedTable.activeTable = this.model;
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model.query = this._context.model('StudentTheses')
        .where('student').equal(params.id)
        .expand('thesis($expand=startYear,instructor,status,type)')
        .prepare();
      this.model.config = AdvancedTableConfiguration.cast(STUDENTS_THESES_LIST_CONFIG);
      this.model.fetch();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
}
