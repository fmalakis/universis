import {LocalUserStorageService, SessionUserStorageService} from './browser-storage.service';
import {AngularDataContext, MostModule} from '@themost/angular';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ApiTestingController} from '../../../testing/src/api-testing-controller.service';
import {ApiTestingModule} from '../../../testing/src/api-testing.module';
import {TestBed, inject, async} from '@angular/core/testing';
import * as _ from 'lodash';
import { FallbackUserStorageService, USER_STORAGE} from './fallback-user-storage.service';
import {UserStorageService} from './user-storage';
import {SESSION_VALUE, BOTH_VALUE, LOCAL_VALUE, NONE_VALUE} from './fallback-user-storage.values';

describe('FallbackUserStorageService with SessionStorage as fallback', () => {
  let mockApi: ApiTestingController;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        ApiTestingModule.forRoot(),
        CommonModule,
        FormsModule,
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [],
      providers: [
        {
          provide: USER_STORAGE,
          useValue: SESSION_VALUE
        },
        FallbackUserStorageService,
        LocalUserStorageService,
        SessionUserStorageService,
        UserStorageService
      ]
    }).compileComponents();
    mockApi = TestBed.get(ApiTestingController);
    mockApi.match({
      url: '/api/diagnostics/services',
      method: 'GET'
    }).map(request => {
      request.flush([
      ]);
    });
  });

  afterEach(async () => {
    localStorage.clear();
    sessionStorage.clear();
  });

  it('should create a new instance', inject([AngularDataContext], (_context: AngularDataContext) => {
    const fallbackUserStorageService = new FallbackUserStorageService(_context, SESSION_VALUE);
    expect(fallbackUserStorageService).toBeTruthy();
  }));

  it('should set item to SessionUserStorage', inject([FallbackUserStorageService], async(fallback: FallbackUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await fallback.setItem(key, value);
    const sessionStorageItem = JSON.parse(sessionStorage.getItem('userSessionStorage'));
    const expectedValue = _.get(sessionStorageItem, key);
    const result = _.has(sessionStorageItem, key);
    expect(result).toBeTruthy();
    expect(expectedValue).toEqual(value);
  }));

  it('should get item from SessionUserStorage', inject([FallbackUserStorageService], async(fallback: FallbackUserStorageService) => {
    expect(await fallback.getItem('key')).toEqual({key: 'key', value: null});
  }));

  it('should remove item from Session Storage', inject([FallbackUserStorageService], async(fallback: FallbackUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await fallback.setItem(key, value);
    let sessionStorageItem = JSON.parse(sessionStorage.getItem('userSessionStorage'));
    let result = _.has(sessionStorageItem, key);
    expect(result).toBeTruthy();
    await fallback.removeItem(key);
    sessionStorageItem = JSON.parse(sessionStorage.getItem('userSessionStorage'));
    result = _.has(sessionStorageItem, key);
    expect(result).toBeFalsy();
  }));

});

describe('FallbackUserStorageService with LocalUserStorage as fallback', () => {
  let mockApi: ApiTestingController;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        ApiTestingModule.forRoot(),
        CommonModule,
        FormsModule,
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [],
      providers: [
        {
          provide: USER_STORAGE,
          useValue: LOCAL_VALUE
        },
        FallbackUserStorageService,
        LocalUserStorageService,
        SessionUserStorageService,
        UserStorageService
      ]
    }).compileComponents();
    mockApi = TestBed.get(ApiTestingController);
    mockApi.match({
      url: '/api/diagnostics/services',
      method: 'GET'
    }).map(request => {
      request.flush([
      ]);
    });
  });

  afterEach(async () => {
    localStorage.clear();
    sessionStorage.clear();
  });

  it('should create a new instance', inject([AngularDataContext], (_context: AngularDataContext) => {
    const fallbackUserStorageService = new FallbackUserStorageService(_context, LOCAL_VALUE);
    expect(fallbackUserStorageService).toBeTruthy();
  }));

  it('should set item to LocalUserStorage', inject([FallbackUserStorageService], async(fallback: FallbackUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await fallback.setItem(key, value);
    const localStorageItem = JSON.parse(localStorage.getItem('userLocalStorage'));
    const expectedValue = _.get(localStorageItem, key);
    const result = _.has(localStorageItem, key);
    expect(result).toBeTruthy();
    expect(expectedValue).toEqual(value);
  }));

  it('should get item from LocalUserStorage', inject([FallbackUserStorageService], async(fallback: FallbackUserStorageService) => {
    expect(await fallback.getItem('key')).toEqual({key: 'key', value: null});
  }));

  it('should remove item from LocalUserStorage', inject([FallbackUserStorageService], async(fallback: FallbackUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await fallback.setItem(key, value);
    let localStorageItem = JSON.parse(localStorage.getItem('userLocalStorage'));
    let result = _.has(localStorageItem, key);
    expect(result).toBeTruthy();
    await fallback.removeItem(key);
    localStorageItem = JSON.parse(localStorage.getItem('userLocalStorage'));
    result = _.has(localStorageItem, key);
    expect(result).toBeFalsy();
  }));

});

describe('FallbackUserStorageService with no browser API as fallback', () => {
  let mockApi: ApiTestingController;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        ApiTestingModule.forRoot(),
        CommonModule,
        FormsModule,
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [],
      providers: [
        {
          provide: USER_STORAGE,
          useValue: NONE_VALUE
        },
        FallbackUserStorageService,
        LocalUserStorageService,
        SessionUserStorageService,
        UserStorageService
      ]
    }).compileComponents();
    mockApi = TestBed.get(ApiTestingController);
    mockApi.match({
      url: '/api/diagnostics/services',
      method: 'GET'
    }).map(request => {
      request.flush([
      ]);
    });
  });

  afterEach(async () => {
    localStorage.clear();
    sessionStorage.clear();
  });

  it('should create a new instance', inject([AngularDataContext], (_context: AngularDataContext) => {
    const fallbackUserStorageService = new FallbackUserStorageService(_context, NONE_VALUE);
    expect(fallbackUserStorageService).toBeTruthy();
  }));

  it('should throw error while setting item to UserStorage', inject([FallbackUserStorageService],
    async(fallback: FallbackUserStorageService) => {
    const key = 'key';
    const value = 'value';
    try {
      await fallback.setItem(key, value);
    } catch (err) {
      expect(err.message).toEqual('No user storage provider was found');
    }
  }));

  it('should throw error while getting item from UserStorage', inject([FallbackUserStorageService],
    async(fallback: FallbackUserStorageService) => {
    try {
      await fallback.getItem('key');
    } catch (e) {
      expect(e.message).toEqual('No user storage provider was found');
    }
  }));

  it('should throw error while removing item from UserStorage', inject([FallbackUserStorageService],
    async(fallback: FallbackUserStorageService) => {
    try {
      await fallback.removeItem('key');
    } catch (e) {
      expect(e.message).toEqual('No user storage provider was found');
    }
  }));

});

describe('FallbackUserStorageService with both browser APIs as fallback', () => {
  let mockApi: ApiTestingController;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        ApiTestingModule.forRoot(),
        CommonModule,
        FormsModule,
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [],
      providers: [
        {
          provide: USER_STORAGE,
          useValue: BOTH_VALUE
        },
        FallbackUserStorageService,
        LocalUserStorageService,
        SessionUserStorageService,
        UserStorageService
      ]
    }).compileComponents();
    mockApi = TestBed.get(ApiTestingController);
    mockApi.match({
      url: '/api/diagnostics/services',
      method: 'GET'
    }).map(request => {
      request.flush([
      ]);
    });
  });

  afterEach(async () => {
    localStorage.clear();
    sessionStorage.clear();
  });

  it('should create a new instance', inject([AngularDataContext], (_context: AngularDataContext) => {
    const fallbackUserStorageService = new FallbackUserStorageService(_context, SESSION_VALUE);
    expect(fallbackUserStorageService).toBeTruthy();
  }));

  it('should set item to SessionUserStorage', inject([FallbackUserStorageService], async(fallback: FallbackUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await fallback.setItem(key, value);
    const sessionStorageItem = JSON.parse(sessionStorage.getItem('userSessionStorage'));
    const expectedValue = _.get(sessionStorageItem, key);
    const result = _.has(sessionStorageItem, key);
    expect(result).toBeTruthy();
    expect(expectedValue).toEqual(value);
  }));

  it('should get item from SessionUserStorage', inject([FallbackUserStorageService], async(fallback: FallbackUserStorageService) => {
    expect(await fallback.getItem('key')).toEqual({key: 'key', value: null});
  }));

  it('should remove item from Session Storage', inject([FallbackUserStorageService], async(fallback: FallbackUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await fallback.setItem(key, value);
    let sessionStorageItem = JSON.parse(sessionStorage.getItem('userSessionStorage'));
    let result = _.has(sessionStorageItem, key);
    expect(result).toBeTruthy();
    await fallback.removeItem(key);
    sessionStorageItem = JSON.parse(sessionStorage.getItem('userSessionStorage'));
    result = _.has(sessionStorageItem, key);
    expect(result).toBeFalsy();
  }));

});
