import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-users-dashboard-overview-groups',
  templateUrl: './users-dashboard-overview-groups.component.html',
  styleUrls: ['./users-dashboard-overview-groups.component.scss']
})
export class UsersDashboardOverviewGroupsComponent implements OnInit {

  @Input() user: any;
  constructor() { }

  ngOnInit() {
  }

}
