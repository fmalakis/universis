import { TestBed } from '@angular/core/testing';

import { AdvancedFormsService } from './advanced-forms.service';
import {AdvancedFormsModule} from './advanced-forms.module';
import {MostModule} from '@themost/angular';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, RouterModule} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';

describe('FormsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
        HttpClientModule,
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/api/',
          options: {
            useResponseConversion: true,
            useMediaTypeExtensions: false
          }
        }),
        AdvancedFormsModule.forRoot(),
        RouterModule.forRoot([])
    ],
    providers: [
      {
        provide: APP_BASE_HREF,
        useValue: '/'
      }
    ]
  }));

  it('should be created', () => {
    const service: AdvancedFormsService = TestBed.get(AdvancedFormsService);
    expect(service).toBeTruthy();
  });
    it('should use FormsService.loadForm()', async () => {
        const service: AdvancedFormsService = TestBed.get(AdvancedFormsService);
        const form = await service.loadForm('LocalDepartments/edit');
        expect(form).toBeTruthy();

        const testComponents = (components) => {
          components.forEach( component => {
            if (component.data && component.data.url) {
              expect(component.data.url).toMatch(/^\/api\//);
            }
            if (component.components) {
              testComponents(component.components);
            }
            if (component.columns) {
              component.columns.forEach( column => {
                if (column.components) {
                  testComponents(column.components);
                }
              });
            }
          });
        };

        testComponents(form.components);
    });

  it('should use QueryParamsPreProcessor.parse()', async () => {
    const activatedRoute: ActivatedRoute = TestBed.get(ActivatedRoute);
    // mock queryParam
    activatedRoute.snapshot.queryParams = {
      param1: '12300030',
      param2: '170'
    };
    const service: AdvancedFormsService = TestBed.get(AdvancedFormsService);
    const form = await service.loadForm('LocalDepartments/edit');
    expect(form.queryParams.param1).toBeTruthy();
    expect(form.queryParams.param2).toBeTruthy();
    expect(form).toBeTruthy();
  });

});
