import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(
    private readonly _context: AngularDataContext
  ) { }

  async getProgramGroups(studentId: number) {
    return this._context.model('StudentProgramGroups')
      .where('student')
      .equal(studentId)
      .expand('programGroup')
      .take(-1)
      .getItems();
  }
}
