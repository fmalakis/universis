import { Component, Input, OnDestroy, OnInit, } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { AppEventService, ModalService, ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';

@Component({
  selector: 'app-edit-course-parts-factors',
  templateUrl: './edit-course-parts-factors.component.html',
})

export class EditCoursePartsFactorsComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy {

  public lastError: any;
  public yesNoOptions = {
    labels: ['Yes', 'No'],
    values: [1, 0]
  };
  @Input() courseParts: any;

  constructor(protected _router: Router,
              protected _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _appEventService: AppEventService) {
    // call super constructor.
    super(_router, _activatedRoute);
    this.modalTitle = this._translateService.instant('Forms.EditPartFactors');
    this.okButtonText = this._translateService.instant('Courses.Submit');
    this.modalClass = 'modal-xl';
  }

  ngOnInit() {
  }

  async ok() {
    // clear error.
    this.lastError = null;
    // validate percentages.
    const validPercantages = this.validatePercentages();
    // convert text to floats.
    this.courseParts.forEach(part => {
      part.coursePartPercent = parseFloat(part.coursePartPercent);
    });
    if (validPercantages) {
      // gather and post.
      const toBeSaved = this.courseParts.map(part => {
        return {
          id: part.id,
          courseStructureType: part.courseStructureType.id,
          calculatedCoursePart: part.calculatedCoursePart,
          coursePartPercent: part.coursePartPercent
        };
      });
      this._context.model('Courses').save(toBeSaved).then(() => {
        // inform user.
        this._toastService.show(this._translateService.instant('Forms.EditFactorsShort'),
          this._translateService.instant('Forms.EditFactorsSuccess'));
        // fire event.
        this._appEventService.change.next({
          model: 'Courses'
        });
        // close.
        if (this._modalService.modalRef) {
          return this._modalService.modalRef.hide();
        }
      }).catch(err => {
        this.lastError = err;
        return false;
      });
    } else {
      // inform user.
      this.lastError = this._translateService.instant('Forms.InvalidPercentages');
      return false;
    }
  }

  ngOnDestroy() {
  }

  async cancel() {
    // restore the parts changed in parent component because of ngModel binding.
    this._appEventService.change.next({
      model: 'Courses'
    });
    // close.
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  validatePercentages(): boolean {
    const validatorObject = {
      percentSum: 0,
      firstPercentValue: this.courseParts[0].coursePartPercent,
      identicalValues: true
    };
    this.courseParts.forEach(part => {
      validatorObject.percentSum += parseFloat(part.coursePartPercent);
      if (part.coursePartPercent !== validatorObject.firstPercentValue) {
        validatorObject.identicalValues = false;
      }
    });
    return (validatorObject.percentSum === 100 || validatorObject.percentSum === 1 || validatorObject.identicalValues);
  }
}
