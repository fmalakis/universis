import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationsHomeComponent } from './registrations-home.component';

describe('RegistrationsHomeComponent', () => {
  let component: RegistrationsHomeComponent;
  let fixture: ComponentFixture<RegistrationsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
