import {async, TestBed} from '@angular/core/testing';
import { CertificateService } from './certificate-service.service';
import {mockCert} from './mock-certificate';
import {X509} from 'jsrsasign';

describe('CertificateService', () => {
  let certificateService: CertificateService;
  let cert = mockCert;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        CertificateService
      ]
    }).compileComponents();
    certificateService = TestBed.get(CertificateService);
  }));

  it('should parse certificate', () => {
    const parsed = certificateService.getX509Certificate(cert.cert);
    expect(parsed).toBeTruthy();
    expect(parsed instanceof X509);
    // parsed.hex is the certificate represented in hexadecimal format.
    // An X509 certificate is a concatenation of hexadecimal values
    // that also contains the certificate's signature. So if the
    // parsing was correct, we would expect the certificate in hex
    // to contain it.
    expect(parsed.hex.includes(cert.signature.toLowerCase())).toBeTruthy();
  });

  it('should return certificate params', () => {
    const X509 = certificateService.getX509Certificate(cert.cert);
    const purposes = certificateService.extractPurposes(X509);
    // This tests that getCertificateParams is called
    // It extends the certificate objects and adds aExtInfo
    // which should be an array of extension attributes of X509.
    expect(X509.aExtInfo).toBeTruthy();
    expect(Array.isArray(X509.aExtInfo)).toBeTruthy();
    expect(purposes).toEqual(cert.purposes);
  });

  it('should get the certificate owner', () => {
    const X509 = certificateService.getX509Certificate(cert.cert);
    const owner = certificateService.extractCertificateOwner(X509);
    const certSubj = X509.getSubjectString();
    expect(certSubj.includes('CN=Universis')).toBeTruthy();
    expect(owner.givenName).toEqual("Universis");
    expect(owner.familyName).toEqual("");
  });
});
