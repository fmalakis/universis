import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService} from '@universis/common';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-graduations-general',
  templateUrl: './graduations-general.component.html'
})
export class GraduationsGeneralComponent implements OnInit {

  public model: any;
  public graduationEventId: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _configurationService: ConfigurationService) { }

  async ngOnInit() {
    this.graduationEventId = this._activatedRoute.snapshot.params.id;

    this.model = await this._context.model('GraduationEvents')
      .where('id').equal(this.graduationEventId)
      .expand('location,studyPrograms,attachmentTypes($expand=attachmentType),reportTemplates')
      .getItem();

    // format dates
    if (this.model && this.model.startDate instanceof Date) {
      const startDate: Date = this.model.startDate;
      const pipe = new DatePipe(this._configurationService.currentLocale);
      this.model.startTime = pipe.transform(startDate, 'HH:mm');
      this.model.startDate = pipe.transform(startDate, 'dd/MM/yyyy');
    } else {
      this.model.startTime = null;
    }

    if (this.model && this.model.validFrom instanceof Date) {
      const pipe = new DatePipe(this._configurationService.currentLocale);
      this.model.validFrom = pipe.transform(this.model.validFrom, 'dd/MM/yyyy');
    }

    if (this.model && this.model.validThrough instanceof Date) {
      const pipe = new DatePipe(this._configurationService.currentLocale);
      this.model.validThrough = pipe.transform(this.model.validThrough, 'dd/MM/yyyy');
    }
  }

}
